CS 490 Professional Bio Webpage

A webpage to display information related to Thomas Yang as a software developer. 
The webpage includes a resume, list of skills, and academic background.

Website URL: https://thomasy1.gitlab.io/professional-bio-homepage/index.html
